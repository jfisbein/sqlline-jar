# Sql Line JAR

Sql Line compiled jar and Postgre SQL drive

## Usage
Copy both files to the host where you want to run it.  
Run as: 
```sh
java -cp sqlline-1.13.0-SNAPSHOT-jar-with-dependencies.jar:postgresql-42.7.1.jar sqlline.SqlLine -d org.postgresql.Driver -u {jdbc_url}
```
